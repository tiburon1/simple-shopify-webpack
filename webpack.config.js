const path = require('path');

const entry =['./theme.js', './theme.scss']

module.exports = {
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  entry,
  output: {
    path: path.resolve(__dirname, 'assets'),
    publicPath: '/',
    filename: 'theme.min.js'
  },
  module: {
    rules: [
      {
	test: /\.scss$/,
	use: [
	  {
	    loader: 'file-loader',
	    options: {
	      name: 'theme.css',
	    }
	  },
	  {
	    loader: 'extract-loader'
	  },
	  {
	    loader: 'css-loader?-url'
	  },
	  {
	    loader: 'postcss-loader'
	  },
	  {
	    loader: 'sass-loader'
	  },
	]
      },
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: 
        { 
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js']
  }
};
