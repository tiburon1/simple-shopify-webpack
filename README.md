# Simple shopify webpack solution

There are many shinier solutions to this problem, but this is the easiest we've found. Just make sure your scss in in `theme.scss` and your js in `theme.js` then run:

```
npm install
npx webpack --watch
```

Now if your're already running themekit you'll have your compiled files uploaded to shopify automatically.

Please fork and do whatever you want with this project.

Made by [Tiburon - Shopify Solutions](https://tiburon.se).

